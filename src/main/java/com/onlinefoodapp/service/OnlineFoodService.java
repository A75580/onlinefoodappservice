package com.onlinefoodapp.service;

import java.util.List;

import com.onlinefoodapp.model.OrderDetails;
import com.onlinefoodapp.model.UserDetails;
import com.onlinefoodapp.request.TransactionHistory;
import com.onlinefoodapp.request.VendorDetails;

public interface OnlineFoodService {

	public UserDetails createUser(UserDetails user);

	public List<OrderDetails> getlatestOrders(long userid);

	public OrderDetails orderfood(TransactionHistory transhistory,VendorDetails vendor, long userid) throws Exception;

}