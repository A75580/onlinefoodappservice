package com.onlinefoodapp.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.onlinefoodapp.client.OnlineBankeServiceClient;
import com.onlinefoodapp.client.VendorClient;
import com.onlinefoodapp.exception.RecordsNotFoundException;
import com.onlinefoodapp.model.OrderDetails;
import com.onlinefoodapp.model.UserDetails;
import com.onlinefoodapp.repository.OrderRepository;
import com.onlinefoodapp.repository.UserRepository;
import com.onlinefoodapp.request.TransactionHistory;
import com.onlinefoodapp.request.VendorDetails;

@Service
@Transactional
public class OnlineFoodServiceImpl implements OnlineFoodService{

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	OrderRepository orderRepository;

	@Autowired
	VendorClient vendorclient;
	
	@Autowired
	OnlineBankeServiceClient onlinebankclient;
	
	@Override
	public UserDetails createUser(UserDetails user) {
		Optional<UserDetails> findByUid=userRepository.findById(user.getId());
		if(findByUid.isPresent()) {
			UserDetails updateuser=findByUid.get();
			updateuser.setId(user.getId());
			updateuser.setGender(user.getGender());
			updateuser.setAge(user.getAge());
			updateuser.setUname(user.getUname());
			return userRepository.save(updateuser);
		}else {
			return userRepository.save(user);
		}
	}

	@Override
	public List<OrderDetails> getlatestOrders(long userid) {
		Pageable top10=PageRequest.of(0, 10);
		List<UserDetails> lstOfrec=  userRepository.findByUsername(userid, top10);
		UserDetails udetails=new UserDetails();
		lstOfrec.stream().forEach(lst->udetails.setOrderlist(lst.getOrderlist()));
		List<OrderDetails> orderdet=udetails.getOrderlist();
		orderdet.stream().forEach(orderlst->System.out.println(""+orderlst.getId()));
		return orderdet;
	}

	@Override
	public OrderDetails orderfood(TransactionHistory transhistory,VendorDetails vendor, long userid) throws Exception {
		Optional<UserDetails> checkuserById=userRepository.findById(userid);
		if(checkuserById.isPresent()) {  //check user existence for place order
		ResponseEntity<List<VendorDetails>> foodlist= vendorclient.searchfood(vendor.getItemname());
			if(foodlist.getBody().size()>0) {
				//makePayment before going to place order
				ResponseEntity<TransactionHistory> thistory=onlinebankclient.paymentofOrder(transhistory);
				if(thistory.getBody().getTransid() !=0) {
				OrderDetails uo=new OrderDetails();
				uo.setAmount(vendor.getPrice());
				uo.setItemname(vendor.getItemname());
				uo.setItmtype(vendor.getItmtype());
				uo.setOrderstatus("ordered");
				uo.setVname(vendor.getVname());
				return orderRepository.save(uo);
				}else {
					throw new RecordsNotFoundException("payment failed check your payment service");
				}
			}else {
				System.out.println("Please select food");
				throw new RecordsNotFoundException("Please select food");
			}
		}else {
			System.out.println("User Not Registered");
			throw new RecordsNotFoundException("User Not Registered/User Not Found");
		}
	}
	
	
	
	
}
