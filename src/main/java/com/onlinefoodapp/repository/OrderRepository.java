package com.onlinefoodapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.onlinefoodapp.model.OrderDetails;
@Repository
public interface OrderRepository extends JpaRepository<OrderDetails, Long> {

}