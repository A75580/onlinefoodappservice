package com.onlinefoodapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

import com.onlinefoodapp.config.RibbonConfiguration;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@RibbonClient(name="onlinefoodappsevice",configuration = RibbonConfiguration.class)
public class OnlineFoodAppServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineFoodAppServiceApplication.class, args);
	}

}